""" random_module

The module contains the random generator and the exceptions that throws.


"""
# from __future__ import annotations
from typing import List, Optional, Tuple
import random
import itertools
import copy


class ParameterNotAListException(Exception):
    """ ParameterNotAListException
    thrown when a parameters who's expected to be a list isn't
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class ListMismatchingSizeException(Exception):
    """ ListMismatchingSizeException
    thrown when two lists do not have the same size.
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class EmptyListException(Exception):
    """ EmptyListException
    thrown when a list is empty.
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class SeedInvalidException(Exception):
    """ SeedInvalidException
    Thrown when the seed is not None or int
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class ProbabilityIncompleteException(Exception):
    """ ProbabilityIncompleteException
    Thrown when the probability list doesn't sum to 1
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class ProbabilityNegativeException(Exception):
    """ ProbabilityNegativeException
    Thrown when a probability is negative
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class NumberGeneratedNotFoundException(Exception):
    """ NumberGeneratedNotFoundException
    This exception should never be raised. It could raise if there is some
    problem in the floating point rounding.
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class RandomGen:
    """ RandomGen
    Given two lists, one with numbers and the second with probabilities,
    pick numbers from the first list with a probability defined by the second.
    """

    def __init__(self,
                 random_numbers: List[float],
                 probabilities: List[float],
                 seed=None) -> None:
        """ RandomGen

        Args:
            random_numbers: list of numbers, float or int
            probabilities: probabilities for each number in random_numbers
            seed: the default (None) delegates to the standard library, an int otherwise

        Raises:
            ParameterNotAListException: if random_numbers or probabilities aren't lists
            ListMismatchingSizeException: if random_numbers and probabilities do not have same size
            SeedInvalidException: if seed is not None or int
            EmptyListException: if random_numbers or probabilities are empty
            TypeError: if any element type of random_numbers is different from int or float
                       if any element type of probabilities is different from float
            ProbabilityIncompleteException: if the probabilities does not sum to 1
            ProbabilityNegativeException: if an element in probabilities is negative
            copy.Error: https://docs.python.org/3/library/copy.html?highlight=copy%20error#copy.Error

        """
        # Values that may be returned by next_num()
        self._random_nums = []
        # Probability of the occurrence of random_nums
        self._probabilities = []

        # test the variables type
        if not isinstance(random_numbers, list):
            raise ParameterNotAListException("random_numbers")

        if not isinstance(probabilities, list):
            raise ParameterNotAListException("probabilities")

        if not random_numbers:
            raise EmptyListException("random_numbers is empty")

        if not probabilities:
            raise EmptyListException("probabilities is empty")

        # test that sizes are the same
        if len(random_numbers) != len(probabilities):
            raise ListMismatchingSizeException(
                "len(random_numbers) != len(probabilities)")

        # set the seed to have repeatable experiments, conversely have it
        # randomly
        if seed is None:
            self._seed = None

        elif isinstance(seed, int):
            self._seed = seed

        else:
            raise SeedInvalidException("seed type is not None or int")

        random.seed(self._seed)

        # check type of elements
        for index in range(len(random_numbers)):

            if not (isinstance(random_numbers[index], int) or
                    isinstance(random_numbers[index], float)):
                raise TypeError("random_numbers at {}".format(index))

            if not isinstance(probabilities[index], float):
                raise TypeError("probabilities at {}".format(index))

            if probabilities[index] < 0.0:
                raise ProbabilityNegativeException(
                    f"negative probability at {index}")

        # check that probabilities sum to 1
        if abs(sum(probabilities) - 1.0) > 0.000001:
            raise ProbabilityIncompleteException(
                "probabilities does not sum to 1 " +
                "(max tolerance 0.000001)")

        # sort decreasing based on probability
        zipped_list = list(zip(probabilities, random_numbers))
        zipped_list.sort(reverse=True)
        tmp_probabilities, tmp_random_numbers = zip(*zipped_list)

        # random nums in constant access time, transform in dictionary
        self._random_nums = {i: v for i, v in enumerate(tmp_random_numbers)}
        self._probabilities = {i: v for i, v in enumerate(
            itertools.accumulate(tmp_probabilities))}

        self._origin_random_nums = copy.deepcopy(random_numbers)
        self._origin_probabilities = copy.deepcopy(probabilities)

    def next_num(self) -> float:
        """    Returns one of the randomNums.
        When this method is called multiple times over a long period,
        it should return the
        numbers roughly with the initialized probabilities.

        --- returns ---
        Returns one of the randomNums.

        --- Exceptions ---
        NumberGeneratedNotFoundException
             It could raise if there is some problem in the floating
             point rounding.
        """
        element = random.random()

        for index in self._probabilities:
            if element <= self._probabilities[index]:
                return self._random_nums[index]

        # in theory this exception should never be raised.
        raise NumberGeneratedNotFoundException(
            f"next_num element searched: {element}")

    def get_numbers(self) -> List[float]:
        """ get_numbers
        returns the list of numbers
        """
        return self._origin_random_nums

    def get_probabilities(self) -> List[float]:
        """ get_probabilities
        returns the list of probabilities for the random numbers
        """
        return self._origin_probabilities

    def get_initial_seed(self) -> Optional[int]:
        """ get_initial_seed

        --- return ---
        int: if a custom seed has been used
        or None if the default has been used
        """
        return self._seed

    @staticmethod
    def get_random_state() -> Tuple:
        """ get_random_state

        --- return ---
        object: current random state of the generator
        """
        return random.getstate()

    @staticmethod
    def set_random_state(state: Tuple) -> None:
        """ set_random_state

        --- parameters ---
        state (object): a random state for the generator returned from
                        get_random_state()

        --- return ---
        None
        """
        random.setstate(state)
