import time
import pytest
from random_module import ParameterNotAListException, ListMismatchingSizeException,\
    SeedInvalidException, RandomGen, ProbabilityIncompleteException, EmptyListException, ProbabilityNegativeException
import sys
sys.path.append("src")


@pytest.fixture(scope="function")
def valid_list_random_numbers_10():
    return [-1, -2, 1, 2, 3, 4, 5, 6, 7, 8]


@pytest.fixture(scope="function")
def valid_list_random_numbers_3():
    return [-1, -2, 1, ]


@pytest.fixture(scope="function")
def lesser_list_probabilities_10():
    return [0.1, 0.2, 0.1, 0.01, 0.02, 0.04, 0.03, 0.09, 0.01, 0.04]


@pytest.fixture(scope="function")
def greater_list_probabilities_10():
    return [0.1, 0.2, 0.1, 0.01, 0.02, 0.04, 0.03, 0.09, 0.01, 0.99]


@pytest.fixture(scope="function")
def invalid_type_list_random_numbers_4():
    return [-1, -2, 1, "prova"]


@pytest.fixture(scope="function")
def invalid_type_list_probability_4():
    return [-1, -2, 1, "prova"]


@pytest.fixture(scope="function")
def invalid_type_list_probability_integer():
    return [-1, -2, 1]


@pytest.fixture(scope="function")
def invalid_negative_list_probability():
    return [0.1, -2, ]


@pytest.fixture(scope="function")
def invalid_sum_list_probability():
    return [0.1, 1]


@pytest.fixture(scope="function")
def ok_number():
    return [-1, 0, 1, 2, 3]


@pytest.fixture(scope="function")
def ok_number_4():
    return [-1, 0, 1, 2]


@pytest.fixture(scope="function")
def wrong_list_type1():
    return 123


@pytest.fixture(scope="function")
def wrong_list_type2():
    return {}


@pytest.fixture(scope="function")
def empty_list_type1():
    return []


@pytest.fixture(scope="function")
def ok_probabilities_4():
    return [0.02, 0.3, 0.58, 0.1]


@pytest.fixture(scope="function")
def negative_probabilities_4():
    return [0.02, 0.3, -0.58, 0.1]


@pytest.fixture(scope="function")
def ok_probabilities():
    return [0.01, 0.3, 0.58, 0.1, 0.01]


@pytest.fixture(scope="function")
def ok_seed():
    return 1203


@pytest.fixture(scope="function")
def bad_seed():
    return "ciasd"


def test_state_is_returned_and_set_up(ok_number, ok_probabilities, ok_seed):
    a = RandomGen(ok_number, ok_probabilities, ok_seed)
    assert a.get_initial_seed() == ok_seed
    state = a.get_random_state()
    b = RandomGen(ok_number, ok_probabilities)
    assert b.get_initial_seed() is None

    b.set_random_state(state)

    assert state == b.get_random_state()
    assert a.get_random_state() == b.get_random_state()


def test_negative_probability(ok_probabilities_4, negative_probabilities_4):
    """
    test negative probability
    """
    with pytest.raises(ProbabilityNegativeException) as excinfo:
        RandomGen(ok_probabilities_4,
                  negative_probabilities_4,)

    assert "negative probability at 2" in str(excinfo.value)


def test_seed_returned_correctly(ok_number, ok_probabilities, ok_seed):
    """
    test seed is returned and setup correctly
    """
    a = RandomGen(ok_number, ok_probabilities, ok_seed)
    assert a.get_initial_seed() == ok_seed


def test_probability_incomplete_less(valid_list_random_numbers_10,
                                     lesser_list_probabilities_10):
    """
    test probability incomplete less than 1
    """
    with pytest.raises(ProbabilityIncompleteException) as excinfo:
        RandomGen(valid_list_random_numbers_10,
                  lesser_list_probabilities_10,)

    assert "probabilities does not sum to 1 (max tolerance 0.000001)" in str(
        excinfo.value)


def test_probability_incomplete_great(valid_list_random_numbers_10,
                                      greater_list_probabilities_10):
    """
    test probability incomplete greater than 1
    """
    with pytest.raises(ProbabilityIncompleteException) as excinfo:
        RandomGen(valid_list_random_numbers_10,
                  greater_list_probabilities_10,)

    assert "probabilities does not sum to 1 (max tolerance 0.000001)" in str(
        excinfo.value)


def test_wrong_seed(ok_number, ok_probabilities, bad_seed):
    """
    test wrong seed
    """
    with pytest.raises(SeedInvalidException) as excinfo:
        RandomGen(ok_number,
                  ok_probabilities,
                  bad_seed)

    assert "seed type is not None or int" in str(excinfo.value)


def test_wrong_list_type1(wrong_list_type1, ok_probabilities_4):
    """
    test not a list type for numbers
    """
    with pytest.raises(ParameterNotAListException) as excinfo:
        RandomGen(wrong_list_type1,
                  ok_probabilities_4)

    assert "random_numbers" in str(excinfo.value)


def test_wrong_list_type2(ok_number_4, wrong_list_type1):
    """
    test not a list type for probabilities
    """
    with pytest.raises(ParameterNotAListException) as excinfo:
        RandomGen(ok_number_4,
                  wrong_list_type1)

    assert "probabilities" in str(excinfo.value)


def test_wrong_list_type3(ok_number_4, wrong_list_type2):
    """
    test not a list type for probabilities
    """
    with pytest.raises(ParameterNotAListException) as excinfo:
        RandomGen(ok_number_4,
                  wrong_list_type2)

    assert "probabilities" in str(excinfo.value)


def test_empty_list(ok_number_4, empty_list_type1):
    """
    test empty list for probabilities
    """
    with pytest.raises(EmptyListException) as excinfo:
        RandomGen(ok_number_4,
                  empty_list_type1)

    assert "probabilities is empty" in str(excinfo.value)


def test_content_wrong_type1(invalid_type_list_random_numbers_4,
                             ok_probabilities_4):
    """
    test a wrong type is identified
    """
    with pytest.raises(TypeError) as excinfo:
        RandomGen(invalid_type_list_random_numbers_4,
                  ok_probabilities_4)

    assert "random_numbers at 3" in str(excinfo.value)


def test_content_wrong_type2(ok_number_4, invalid_type_list_probability_4):
    """
    test a wrong type in probabilities
    """
    with pytest.raises(TypeError) as excinfo:
        RandomGen(ok_number_4,
                  invalid_type_list_probability_4)

    assert "probabilities at 0" in str(excinfo.value)


def test_OK(ok_number, ok_probabilities):
    """
    test that a valid input is accepted with default seed
    """
    RandomGen(ok_number, ok_probabilities)


def test_OK_with_seed(ok_number, ok_probabilities, ok_seed):
    """
    test that a valid input is accepted
    """
    RandomGen(ok_number, ok_probabilities, ok_seed)


def test_probability_incomplete(valid_list_random_numbers_10,
                                lesser_list_probabilities_10):
    """
    test that the probability is incomplete is raised, this case is less than 1
    """
    with pytest.raises(ProbabilityIncompleteException) as excinfo:
        RandomGen(valid_list_random_numbers_10, lesser_list_probabilities_10)

    assert "probabilities does not sum to 1 (max tolerance 0.000001)" in str(
        excinfo.value)


def test_fail_size(valid_list_random_numbers_3, lesser_list_probabilities_10):
    """
    test that if the list size mismatch raises the right exception
    """
    with pytest.raises(ListMismatchingSizeException) as excinfo:
        RandomGen(valid_list_random_numbers_3, lesser_list_probabilities_10)

    assert "len(random_numbers) != len(probabilities)" in str(
        excinfo.value)


def test_speed(ok_number, ok_probabilities):
    """
    test speed of the random generation, arbitrary one milion in a second
    """
    gen = RandomGen(ok_number, ok_probabilities)

    start = time.time()
    for _ in range(1000000):
        gen.next_num()
    end = time.time()

    assert end-start < 1, "getting slow.. 1m random draws in < 1s fails"
